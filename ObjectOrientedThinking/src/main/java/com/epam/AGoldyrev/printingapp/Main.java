package com.epam.AGoldyrev.printingapp;

import com.epam.AGoldyrev.printingapp.model.Document;
import com.epam.AGoldyrev.printingapp.model.Printer;
import com.epam.AGoldyrev.printingapp.model.Resources;

public class Main {
    public static void main(String[] args) {
        Printer printer = new Printer(new Resources(7,8), new Document("doc",5, "vertical"));
        System.out.println(printer.printDoc());
    }
}