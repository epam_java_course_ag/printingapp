package com.epam.AGoldyrev.printingapp.model;

public class Document {
    private String format;
    private int numberOfPages;
    private String orientation;

    public Document(String format, int numberOfPages, String orientation) {
        this.format = format;
        this.numberOfPages = numberOfPages;
        this.orientation = orientation;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }
}
