package com.epam.AGoldyrev.printingapp.model;

public enum Status { READY_TO_PRINT, OUT_OF_INK, OUT_OF_PAPER, IN_PROGRESS}
