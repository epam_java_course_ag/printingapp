package com.epam.AGoldyrev.printingapp.model;

public class Resources {
    private int ink;
    private int paper;

    public Resources(int ink, int paper) {
        this.ink = ink;
        this.paper = paper;
    }

    public int getInk() {
        return ink;
    }

    public void setInk(int ink) {
        this.ink = ink;
    }

    public int getPaper() {
        return paper;
    }

    public void setPaper(int paper) {
        this.paper = paper;
    }
}
