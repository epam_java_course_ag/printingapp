package com.epam.AGoldyrev.printingapp.model;

public class Printer {
    private Resources resources;
    private Document document;

    public Printer(Resources resources, Document document) {
        this.resources = resources;
        this.document = document;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }


    private Status checkStatus(){
        if( resources != null && resources.getInk() == 0){
            return Status.OUT_OF_INK;
        }

        if(resources != null && resources.getPaper() == 0){
            return Status.OUT_OF_PAPER;
        }
        if (resources != null && resources.getInk() > 0 && resources.getPaper() > 0 && document.getNumberOfPages() == 0){
            return Status.READY_TO_PRINT;
        }
        return Status.IN_PROGRESS;
    }

    public boolean printDoc(){
        while (checkStatus() == Status.IN_PROGRESS){
            decreaseRes();
            System.out.println("page printed");

        }
        if(checkStatus() == Status.READY_TO_PRINT){
            return true;
        }
        return false;
    }

    private void decreaseRes(){
        int valueInk =resources.getInk();
        valueInk = valueInk -1;
        resources.setInk(valueInk);

        int valuePaper =resources.getPaper();
        valuePaper = valuePaper -1;
        resources.setPaper(valuePaper);

        int valuePages = document.getNumberOfPages();
        valuePages = valuePages -1;
        document.setNumberOfPages(valuePages);

    }
}
